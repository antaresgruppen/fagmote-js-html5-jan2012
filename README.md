Imported from https://github.com/AntaTom/fagmote-2011-10

## Prerequisites
Before hands-on session all installation should be ready. Please download or check out this project and run mvn clean install. Then see that you can get tomcat or jetty to serve you the page at http://localhost:8080/fagmote/   
*	Computer with Chrome or FireFox with FireBug   
*	Maven 2   
*	Text editor   
*	Windows users: tomcat   
*	Optional: git-client   
*	Optional: IDE, from list at [Stack Overflow] I collected:   
-		[WebStorm](http://www.jetbrains.com/webstorm/) - I liked it   
-		[Aptana](http://www.aptana.com/) - I didn't like it   


## Quickstart
	1. Pull (check out) from https://github.com/AntaTom/fagmote-2011-10   
	2. mvn clean install   
	3. *nix: Jetty   
		3.1. Start jetty (with "mvn jetty:run")   
	3. Windows: Tomcat   
		3.1. mvn war:inplace   
		3.2. Edit fagmote.xml and put in tomcat context dir (conf/Catalina/localhost)   
		3.3. Start tomcat   
	4. Go to [index file](http://localhost:8080/fagmote/index.html)   
	5. Further instructions in [index file](http://localhost:8080/fagmote/index.html)   


## Maven CLI
"mvn clean install" should work out of the box, if FireFox installed to default location.
Otherwise:   
pom specifies location of FireFox and Chrome binaries in properties, override as appropriate


## Eclipse (with m2eclipse):
Import Maven project   
class JettyConfig can be used to run / debug web-application


## jQuery (jquery.com)
JavaScript Library that simplifies:

 * html dom manipulation, with animations and all

 * ajax calls to server

Accessed through $ function

Documented on http://docs.jquery.com/Main_Page

Of particular interest are references on: Selectors, Manipulation and Ajax


## DOM manipulation:
Given html: &lt;p id="greeting" >&lt;/p>

This javascript: $('#greeting').append( 'Hello World' )

Yields modified html: &lt;p id="greeting" >Hello World&lt;/p>

... that results in user seeing "Hello World"


## DOM traversal:
Given html: &lt;input type="text" name="text" id="textfield" />

This javascript: $('#textfield').val()

Yields the text entered (by user) in textfield

## Ajax:
The simplest variant:

	$.get(
		"service-url",	// url of service
		function(data) {	// callback function to receive response
			alert( data )
		}
	);


Remember that JavaScript must keep withn originating site

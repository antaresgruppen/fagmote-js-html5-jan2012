package no.antares.kickstart.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	public static final String DATE_ONLY_FORMAT = "dd-MMM-yyyy";
    public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";

    /** convert string with format DATE_ONLY_FORMAT (dd.MM.yyyy) */
    public static Date now() {
    	return new Date();
    }

    /** convert string with format DATE_ONLY_FORMAT (dd.MM.yyyy) */
    public static Date dateOnlyString2date(String date) throws ParseException {
        if (date == null)
            return null;
        // midday to avoid being between dates
        return parseDate( date + " 12:00:00", DATE_TIME_FORMAT );
    }

    /** convert string with format DATE_ONLY_FORMAT */
    public static Date dateTimeString2date(String date) throws ParseException {
        if (date == null)
            return null;
        return parseDate( date, DATE_TIME_FORMAT );
    }
    
    /** gives a string with format DATE_TIME_FULL_FORMAT */
    public static String dateOnlyString(java.util.Date date){
        if (date == null)
            return "";
        return formatDate( date, DATE_ONLY_FORMAT );
    }
    
    /** gives a string with format DATE_TIME_FULL_FORMAT */
    public static String dateTimeString(java.util.Date date){
        if (date == null)
            return "";
        return formatDate( date, DATE_TIME_FORMAT );
    }

    /** Formats a date according to a dateFormat string */
    private static String formatDate( Date date, String dateFormat ) {
    	DateFormat formatter = new SimpleDateFormat( dateFormat );
    	return formatter.format( date );
    }

    /** Parses a string into a date according to a given dateFormat */
    private static Date parseDate( String dateString, String dateFormat ) throws ParseException {
    	DateFormat formatter = new SimpleDateFormat( dateFormat );
    	return formatter.parse( dateString );
    }

}

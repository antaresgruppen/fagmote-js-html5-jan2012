function Timereg() {
	var dates = [];
	
	this.getDates = function() {
		return dates;
	}
	
	this.addDate = function(d) {
		if (!this.containsDate(d)) {
			dates.push(d);
		}
	};
	
	this.containsDate = function(d) {
		var arLen = dates.length;
		for ( var i=0, len = arLen; i < len; ++i ){
			var eq = dates[i].equals(d);
			if (eq) { return true; }
		}
		return false;
	};
	
	this.getDate = function(d) {
		var arLen = dates.length;
		for ( var i=0, len = arLen; i < len; ++i ){
			var eq = dates[i].getDate().equals(d);
			if (eq) { return dates[i]; }
		}
		var rDate = new RegDate(d);
		dates.push(rDate);
		return rDate;
	};
	this.toJSON = function() {
		/*var out = '';
		var arLen = dates.length;
		for ( var i=0, len = arLen; i < len; ++i ){
			out += dates[i].toString();
			if (i < len - 1) out += ',';
		}
		return out;*/
		return '\'dates\':' + JSON.stringify(dates);
	};
}

function RegDate(d) {
	var date = d;
	var entries = [];
	
	this.addEntry = function(entry) {
		if (!this.containsEntry(entry)) {
			entries.push(entry);
		}
	};
	
	this.getEntries = function() {
		return entries;
	};
	
	this.getDate = function() {
		return date;
	};
	
	this.equals = function(other) {
		return date.getTime() == other.getDate().getTime();
	};
	
	this.getEntry = function(key) {
		var arLen = entries.length;
		for ( var i=0, len = arLen; i < len; ++i ){
			var eq = entries[i].getProject() == key;
			if (eq) { return entries[i]; }
		}
		var e = new Entry(key, 0);
		entries.push(e);
		return e;
	};

	this.containsEntry = function(n) {
		var arLen = entries.length;
		for ( var i=0, len = arLen; i < len; ++i ){
			var eq = entries[i].equals(n);
			if (eq) { return true; }
		}
		return false;
	};
	
	this.toJSON = function() {
		return '\'date\':\'' + date + '\',\'entries\':' + JSON.stringify(entries);
	};
}

/*Date.prototype.addEntry = function(entry) {
	var entr = [];
	if (this.entries == null) {
		this.entries = entr;
	}
	this.entries.push(entry);
};

Date.prototype.getEntries = function() {
	return this.entries;
};*/

function Entry(project, hours) {
	var proj = project;
	var hrs = hours;
	
	this.getProject = function() {
		return proj;
	}
	this.getHours = function() {
		return hrs;
	}
	this.setHours = function(hours) {
		hrs = hours;
	}
	this.toJSON = function() {
		var json = "{" + "proj".toJSON() + ":'" + proj + "'," + "hrs".toJSON() + ":'" + hrs + "'}";
		return json;
		/*var json = [];
		for (var i in this){
			if(!this.hasOwnProperty(i)) continue;
			//if(typeof this[i] == "function") continue;
			json.push(i.toJSON() + " : " +
				((this[i] != null) ? this[i].toJSON() : "null")
			)
		}
		return "{\n " + json.join(",\n ") + "\n}";*/
	}
}

String.prototype.toJSON = function() {
	return '"' +
		this.replace(/(\\|\")/g,"\\$1")
		.replace(/\n|\r|\t/g, function(){
			var a = arguments[0];
			return  (a == '\n') ? '\\n':
					(a == '\r') ? '\\r':
					(a == '\t') ? '\\t': ""
		}) +
		'"'
}
